package pw.pw;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

public class SparingAdapter extends RecyclerView.Adapter<SparingAdapter.SparingViewHolder> {

    public ISparingAdapter sparingAdapter;
    private Context mCtx;
    private List<Sparing> sparingList;

    public SparingAdapter(Context mCtx, List<Sparing> sparingList) {
        this.mCtx = mCtx;
        this.sparingList = sparingList;
        sparingAdapter = (ISparingAdapter) mCtx;
    }

    @NonNull
    @Override
    public SparingAdapter.SparingViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater inflater = LayoutInflater.from(mCtx);
        View view = inflater.inflate(R.layout.list_item, null);
        return new SparingViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull SparingAdapter.SparingViewHolder holder, int position) {
        Sparing sparing = sparingList.get(position);

        holder.olahraga.setText(sparing.getOlahraga());
        holder.count.setText(sparing.getCount());

    }

    @Override
    public int getItemCount() {
        return sparingList.size();
    }

    public interface ISparingAdapter {
        void detail(int pos);
    }


    class SparingViewHolder extends RecyclerView.ViewHolder {

        TextView olahraga, count;

        public SparingViewHolder(View itemView) {

            super(itemView);

            olahraga = itemView.findViewById(R.id.olahraga);
            count = itemView.findViewById(R.id.count);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                }
            });
        }
    }

}
