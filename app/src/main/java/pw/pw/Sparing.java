package pw.pw;

import java.io.Serializable;

public class Sparing implements Serializable {

    int id;
    String olahraga, count;

    public Sparing() {

    }

    public Sparing(int id, String olahraga, String count) {
        this.id = id;
        this.olahraga = olahraga;
        this.count = count;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getOlahraga() {
        return olahraga;
    }

    public void setOlahraga(String olahraga) {
        this.olahraga = olahraga;
    }

    public String getCount() {
        return count;
    }

    public void setCount(String count) {
        this.count = count;
    }
}

